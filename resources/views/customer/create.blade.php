@extends('app')

@section('title', 'create')

@section('content')
    <h1>Create a new customer</h1>
    <form action="/customers" method="post">
        @include('customer.form')
        <button type="submit">Add new</button>
    </form>
    @endsection
