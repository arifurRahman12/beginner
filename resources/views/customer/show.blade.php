<h1>Customer Details</h1>
<div><a href="/customers">< Back</a></div>
<div>
    <p><strong>Name</strong></p>
    {{ $customer->name }}
</div>

<div>
    <p><strong>Email</strong></p>
    {{ $customer->email }}
</div>
<div>
    <a href="/customers/{{ $customer->id }}/edit">Edit Information</a>
    <form action="/customers/{{ $customer->id }}" method="POST">
        @method("DELETE")
        @csrf
        <button>Delete</button>
    </form>
</div>
