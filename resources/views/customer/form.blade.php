@csrf
<div>
    <label for="name">Name</label>
    <input type="text" name="name" autocomplete="off" value="{{ old('name') ?? $customer->name }}">
</div>
@error('name') {{ $message }} @enderror

<div>
    <label for="name">Email</label>
    <input type="text" name="email" autocomplete="off" value="{{ old('email') ?? $customer->email }}">
</div>
@error('email') {{ $message }} @enderror
