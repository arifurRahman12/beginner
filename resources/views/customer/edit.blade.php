@extends('app')

@section('title', 'edit')

@section('content')
    <h1>Edit Customer</h1>
    <form action="/customers/{{ $customer->id }}" method="post">
        @method("PUT")
        @include('customer.form')
        <button type="submit">update</button>
    </form>
@endsection
