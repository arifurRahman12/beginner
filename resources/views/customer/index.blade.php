<h1>Customers</h1>
    <a href="/customers/create">Add a new customer</a>
    <a href="/customers?active=1">active</a>
    <a href="/customers?active=0">inactive</a>
@forelse($customers as $customer)
    <p><a href="/customers/{{ $customer->id }}">
        <strong>{{ $customer->name }}</strong>
    </a> - {{ $customer->email }}</p>

    @empty
    <h3>No customer found</h3>
    @endforelse
