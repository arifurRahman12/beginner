@extends('app')
@section('title', 'Create new servive')
@section('content')
    <form action="/service" method="post">
        @csrf
        <input type="text" name="name" autocomplete= off>
        <button type="submit">Add service</button>
    </form>
        <p>@error('name') {{ $message }} @enderror</p>
    @endsection
