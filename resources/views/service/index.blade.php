@extends('app')

@section('title', 'services')

@section('content')
    <ul>
        @forelse($services as $service)
            <li>{{ $service->name }}</li>
            @empty
            <p>No service available</p>
            @endforelse
    </ul>
    @endsection
