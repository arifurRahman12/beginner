<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Service;

class HelloController extends Controller
{
    public function index()
    {
        $data = 'hello from the controller';
        return view('about', compact('data'));
    }

}
