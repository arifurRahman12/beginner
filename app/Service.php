<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
//    option 1
//    protected $fillable = ['name'];
//    option 2
    protected $guarded = [];
}
